# nexonEV

Community-interaction platform for NexCruise for NexonEV. 

Visit the "issues > Boards" section, and start discussing and logging your ideas!

## Preamble

Dear friends,

Now that there are enough NexCruises out there among all of us, and the pre-orders production looking pretty much on-time, let's go back to brainstorming, improving the product, and think of new features that all of us want in the NexCruise!

While I'm not promising that we will be able to bring out all your ideas and feature requests to NexCruise, but it is time to give a structure to all the creativity here!

All the NexCruise owners - come write your ideas, feature requests, and issues that you're facing, on the NexCruise board (link given below). You can also discuss and comment on the existing ideas here. The Aha team will keep prioritising and picking new features from this idea-soup and delivering them to your NC via OTA updates!

Check out the NexCruise board here:
https://gitlab.com/nexcruise/nexonev/-/boards

## Framework

1) While the board is open for all to read, only NexCruise owners can raise new features requests and discuss the ideas on the board.

2) This is the only official platform which will define the future of NexCruise technology. You can directly shape NexCruise by being active here!

3) This board contains all issues as well as new feature-requests.

4) You will also get to know how/when the issues are getting addressed, what features are coming in the next release, etc.

5) Treat it like an experiment in co-creation, your idea may or may not get implemented, but don't let that stop you from thinking and sharing!!!

## Steps to join

If you are a NexCruise owner, create a free account on gitlab.com, and DM/whatsapp/email me (aakash) your gitlab username. I will add you to the board and you can start interacting right away!

If you don't own a NexCruise yet, you are welcome to still access the board and see how NexCruise is evolving, and what is the team Aha focussing on.
